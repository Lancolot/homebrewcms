<?php
/**
 * Register.php - renders a user registration form
 * 
 * @author Bugslayer
 * 
 */
?>
<form name="input" action="?action=save&page=register" method="post"
	style="width: 850px; margin-left: auto; margin-right: auto">
	<h1>Registreer Nieuwe Gebruiker</h1>
	<p>Vul dit formulier in om een nieuwe gebuiker in te kunnen laten
		loggen op de @@project site</p>
	<input type="hidden" name="send" value="true" />
	<table style="width:850px">
		<tr>
			<td width="230px"><label for="Userid">Gebruikersnaam *</label></td>
			<td width="265px"><input type="text" id="Userid" name="Userid"
				maxlength="50" size="30"></td>
			<td><span id="UseridValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Password">Wachtwoord *</label></td>
			<td><input type="password" id="Password" name="Password" size="30"
				maxlength="30"></td>
			<td><span id="PasswordValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="PwdRetype">Herhaal wachtwoord *</label></td>
			<td><input type="Password" id="PwdRetype" name="PwdRetype" size="30"
				maxlength="30"></td>
			<td><span id="PwdRetypeValResult"> </span></td>
		</tr>
		<tr>
			<td width="230px"><label for="Name_first">Voornaam *</label></td>
			<td width="265px"><input type="text" id="Name_first"
				name="Name_first" maxlength="50" size="30"></td>
			<td><span id="Name_firstValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Name_middle">Tussenvoegsel</label></td>
			<td><input type="text" id="Name_middle" name="Name_middle"
				maxlength="50" size="30"></td>
			<td><span id="Name_middleValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Name_last">Achternaam *</label></td>
			<td><input type="text" id="Name_last" name="Name_last" maxlength="50"
				size="30"></td>
			<td><span id="Name_lastValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Email">Email Adres *</label></td>
			<td><input type="text" id="Email" name="Email" maxlength="80"
				size="30"></td>
			<td><span id="EmailValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Telephone">Telefoonnummer</label></td>
			<td><input type="text" id="Telephone" name="Telephone" maxlength="50"
				size="30"></td>
			<td><span id="TelephoneValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_street">Adres straat</label></td>
			<td><input type="text" id="Address_street" name="Address_street"
				maxlength="50" size="30"></td>
			<td><span id="Address_streetValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_number">Adres straat</label></td>
			<td><input type="text" id="Address_number" name="Address_number"
				maxlength="50" size="30"></td>
			<td><span id="Address_numberValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_zipcode">Adres postcode</label></td>
			<td><input type="text" id="Address_zipcode" name="Address_zipcode"
				maxlength="50" size="30"></td>
			<td><span id="Address_zipcodeValResult"> </span></td>
		</tr>
		<tr>
			<td><label for="Address_city">Adres plaatsnaam</label></td>
			<td><input type="text" id="Address_city" name="Address_city"
				maxlength="50" size="30"></td>
			<td><span id="Address_cityValResult"> </span></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center"><input type="submit"
				value="Verstuur"></td>
		</tr>
	</table>
</form>
