<?php
/**
 * Account.php - component that renders the account menu on top-lleft of the page.
 * 
 * @author Bugslayer
 * 
 */

/**
 * Checkes wether the request is sent by an authenticated user
 *
 * @return TRUE if the request is sent by an authenticated user, otherwise FALSE
 */
function isAuthenticated() {
	if (! isset ( $_SESSION )) {
		return false;
	}
	return isset ( $_SESSION ['loggedin'] );
}

/**
 * Returns a readable name for the authenticated user
 *
 * @return a readable name for the authenticated user. If the request is not sent by an authenticated user, some
 *         warning message is returned.
 */
function getAuthenticatedUsername() {
	if (! isAuthenticated ()) {
		return "<USER NOT AUTHENTICATED>";
	}
	return $_SESSION ['readablename'];
}

?>
<ul class="navbar-top">
	<?php
	if (isAuthenticated ()) {
		echo '<li><a href="">Aangemeld: ' . getAuthenticatedUsername () . '</a>';
		echo '<ul><li><a href="?action=logoff&page=login">Afmelden</a></li></ul>';
		echo '</li>';
	} else {
		echo '<li><a href="?action=show&page=login">Log in</a></li></ul>';
	}
	?>
</ul>
<!-- @@project -->

